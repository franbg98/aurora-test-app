# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

linkedin = Company.create name: 'Linkedin'
aurora = Company.create name: 'Aurora'

Perk.create description: 'TE ENTREVISTA ALGUIEN DE RRHH', company: linkedin
Perk.create description: 'NO TIENEN BUEN CAFÉ', company: linkedin

Perk.create description: 'TE ENTREVISTA ELON MUSK', company: aurora
Perk.create description: 'SON UNOS FRIKIS DEL CAFÉ', company: aurora

current_testimonial = Testimonial.create(quote: 'Confío en aurora para conseguir el mejor talento', name: 'Elon Musk')
current_testimonial.image.attach(io: File.open("#{Rails.root}/app/assets/images/elonmusk.png"), filename: "elonmusk.png")
current_testimonial.save

current_testimonial = Testimonial.create(quote: 'Si no puedes con el enemigo... ¡Cómpralo!', name: 'Bill Gates')
current_testimonial.image.attach(io: File.open("#{Rails.root}/app/assets/images/billgates.jpg"), filename: "billgates.jpg")
current_testimonial.save

current_testimonial = Testimonial.create(quote: 'Tu marca es lo que la gente dice de ti cuando no estas presente', name: 'Jeff Bezos')
current_testimonial.image.attach(io: File.open("#{Rails.root}/app/assets/images/jeffbezos.jpg"), filename: "jeffbezos.jpg")
current_testimonial.save