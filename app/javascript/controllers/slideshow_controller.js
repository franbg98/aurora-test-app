import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "slide", "previous", "next" ]
  static values = { current: Number }

  next() {
    this.currentValue++
  }

  previous() {
    this.currentValue--
  }

  currentValueChanged() {
    this.showCurrentSlide()
    this.buttonVisibility()
  }

  buttonVisibility() {
    this.nextTarget.hidden = this.slideTargets.length - 1 === this.currentValue
    this.previousTarget.hidden = this.currentValue === 0
  }

  showCurrentSlide() {
    this.slideTargets.forEach((element, i) => {
      element.hidden = i != this.currentValue
    })
  }
}