const colors = require('tailwindcss/colors')

module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    colors: {
      white: '#FFF',
      black: '#000',
      blue: {
        lightest: '#DBEAFE',
        light: '#93C5FD',
        DEFAULT: '#60A5FA',
        dark: '#3B82F6',
        darkest: '#1D4ED8',
      },
      pink: {
        light: '#ff7ce5',
        DEFAULT: '#ff49db',
        dark: '#ff16d1',
      },
      gray: {
        darkest: '#1F2937',
        dark: '#4B5563',
        DEFAULT: '#9CA3AF',
        light: '#D1D5DB',
        lightest: '#F3F4F6',
      },
      green: {
        darkest: '#064E3B',
        dark: '#047857',
        DEFAULT: '#10B981',
        light: '#34D399',
        lightest: '#6EE7B7',
      },
      yellow: {
        darkest: '#92400E',
        dark: '#D97706',
        DEFAULT: '#F59E0B',
        light: '#FBBF24',
        lightest: '#FCD34D',
      },
      red: {
        darkest: '#991B1B',
        dark: '#DC2626',
        DEFAULT: '#EF4444',
        light: '#F87171',
        lightest: '#FECACA',
      }
    },
    fontFamily: {
      bebas: ['Bebas Neue', 'sans-serif'],
      roboto: ['Roboto', 'sans-serif'],
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
