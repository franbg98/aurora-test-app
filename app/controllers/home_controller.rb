class HomeController < ApplicationController
		before_action :authenticate_user!
		
    def index
        @companies = Company.all
        @testimonials = Testimonial.all
    end
end
