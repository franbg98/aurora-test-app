class Company < ApplicationRecord
  has_many :perks, dependent: :destroy
end
